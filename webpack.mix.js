const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
const tailwindcss = require("tailwindcss");

mix
	.react()
	.js("resources/js/index.js", "public/js/index.js")
	.js("resources/js/order.js", "public/js/order.js")
	.js("resources/js/react/Checkout/index.jsx", "public/js/checkout.js")
	.sass("resources/css/checkout.scss", "public/css/checkout.css", {}, [
		tailwindcss,
	])
	.postCss("resources/css/app.css", "public/css", [tailwindcss]);
