module.exports = {
	content: [
		"./resources/**/*.blade.php",
		"./resources/**/*.js",
		"./resources/**/*.jsx",
	],
	theme: {
		extend: {
			fontFamily: {
				serif: ["Lora", "ui-serif", "serif"],
				sans: ["Roboto", "ui-serif", "serif"],
				icons: "Material Icons",
			},
		},
	},
	plugins: [],
};
