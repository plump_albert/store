<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public $fillable = ['email','first_name', 'last_name','address'];

    public function products()
    {
        $this->belongsToMany(OrderProducts::class, 'order_products', 'id', 'product_id');
    }
}
