<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Models\Order;
use App\Models\OrderProducts;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.checkout');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCheckoutRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequest $request)
    {
        $data = $request->validated();
        $order = Order::create($data);
        $order->save();
        foreach ($data['products'] as $product) {
            OrderProducts::create(
                ['product_id' => $product['id'],
                'order_id' => $order->id,
                'count' => $product['count']]
            )->save();
        }
        return response()->json(
            ['success' => true,
            'id' => $order->id]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Checkout $checkout
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::firstWhere(['id' => $id]);
        RabbitMQController::sendData($order);
        return view('pages.success', ['order' => $order]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Checkout $checkout
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
