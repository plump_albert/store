<?php

namespace App\Http\Controllers;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exchange\AMQPExchangeType;

/**
 * Contains methods for interacting with RabbitMQ server
 */
class RabbitMQController extends Controller
{
    /**
     * Connection to the RabbitMQ server
     *
     * @var AMQPStreamConnection
     */
    private static $connection;

    /**
     * Initializes connection to RabbitMQ server
     *
     * @return void
     */
    static function __init()
    {
        RabbitMQController::$connection = new AMQPStreamConnection(
            env('RABBITMQ_HOST'),
            env('RABBITMQ_PORT'),
            env('RABBITMQ_USER'),
            env('RABBITMQ_PASSWORD'),
            env('RABBITMQ_VHOST'),
        );
    }

    /**
     * Sends data through RabbitMQ message bus
     *
     * @param mixed $data Associative array with data
     *
     * @return void
     */
    static function sendData($data)
    {
        $channel = RabbitMQController::$connection->channel();
        $channel->exchange_declare('orders', AMQPExchangeType::FANOUT, false, false, false);
        $message = new AMQPMessage(
            json_encode($data),
            array('content_type' => 'application/json')
        );
        $channel->basic_publish($message, 'orders');
        $channel->close();
    }
}

RabbitMQController::__init();
