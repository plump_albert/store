# Simple web-store project

## ROADMAP

- [ ] Create products:
  - [ ] Define product model;
  - [ ] Display products;
- [ ] Implement checkout of products;
- [ ] Pass info for delivery service.
