/**
 * Adds product into localStorage cart
 * @param {number} productId Product ID to add
 */
function addToCart(productId) {
	let products = localStorage.getItem("products")
		? JSON.parse(localStorage.getItem("products"))
		: {};
	if (products[productId]) {
		products[productId] += 1;
	} else {
		products[productId] = 1;
	}
	localStorage.setItem("products", JSON.stringify(products));
	updateCartCount();
}
window.addToCart = addToCart;

/**
 * Deletes product from cart
 * @param {number} productId ID of product to delete
 */
function removeFromCart(productId) {
	let products = localStorage.getItem("products")
		? JSON.parse(localStorage.getItem("products"))
		: null;
	if (!products || !products[productId]) {
		throw {error: true, message: "Product is not in your cart!"};
	}
	products[productId] -= 1;
	if (products[productId] === 0) {
		delete products[productId];
	}
	localStorage.setItem("products", JSON.stringify(products));
	updateCartCount();
}
window.removeFromCart = removeFromCart;

/** @type {HTMLSpanElement} */
let cartIconElement = document.querySelector("#cart-icon");
/**
 * Callback for updating current number of items inside cart
 */
function updateCartCount() {
	let products = localStorage.getItem("products")
		? JSON.parse(localStorage.getItem("products"))
		: {};
	const cartItemsCount = Object.keys(products).reduce(
		(sum, key) => (sum += products[key]),
		0
	);
	cartIconElement.dataset["count"] = cartItemsCount;
}
updateCartCount();

/**
 * Callback for product cart icon click
 * @param {HTMLSpanElement} target
 * @param {number} productId
 */
function productCartClick(target, productId) {
	const isAdded = target.innerHTML.trim() !== "add_shopping_cart";
	if (!isAdded) {
		window.addToCart(productId);
		target.innerText = "remove_shopping_cart";
	} else {
		window.removeFromCart(productId);
		target.innerText = "add_shopping_cart";
	}
}
window.productCartClick = productCartClick;

const productIcons = document.querySelectorAll(".product-icon");
let cartContents = localStorage.getItem("products");
if (cartContents) {
	cartContents = JSON.parse(cartContents);
	for (let icon of productIcons) {
		const id = icon.dataset["id"];
		if (cartContents.hasOwnProperty(id)) {
			icon.innerText = "remove_shopping_cart";
		}
	}
}
