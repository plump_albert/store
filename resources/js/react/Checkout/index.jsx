import React, {useMemo, useReducer} from "react";
import ReactDOM from "react-dom/client";
import Product from "./Product";
import CheckoutReducer, {initializeState} from "./CheckoutReducer.js";

const CheckoutPage = () => {
	const [products, dispatch] = useReducer(
		CheckoutReducer,
		localStorage.getItem("products"),
		initializeState
	);
	const totalPrice = useMemo(
		() =>
			products.reduce(
				(sum, product) => sum + (product.price || 0) * product.count,
				0
			),
		[products]
	);

	if (!products || Object.keys(products).length === 0) {
		return (
			<div className="flex flex-col m-auto text-center">
				<p className="text-black"> Корзина пуста! </p>
				<a href="/" className="text-base text-rose-600 font-medium">
					Вернуться к покупкам
				</a>
			</div>
		);
	}
	return (
		<div className="flex flex-col gap-6">
			<div className="flex flex-col gap-6">
				{products.map(product => (
					<Product
						key={`product-${product.id}`}
						dispatch={dispatch}
						{...product}
					/>
				))}
			</div>
			<hr className="border-gray-200" />
			<p className="flex flex-row justify-between w-full text-base">
				<span className="text-black uppercase font-medium font-sans">
					Итого:
				</span>
				<span>{totalPrice.toFixed(2)} &#8381;</span>
			</p>
			<a
				href="/order"
				class="mt-6 rounded-lg bg-rose-600 py-2 px-4 text-center font-sans font-medium uppercase text-white"
			>
				Оформить
			</a>
		</div>
	);
};

ReactDOM.createRoot(document.querySelector(".product-list")).render(
	<CheckoutPage />
);
