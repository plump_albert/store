const ActionTypes = {
	INCREASE_COUNT: "INCREASE_COUNT",
	DECREASE_COUNT: "DECREASE_COUNT",
	REMOVE_ITEM: "REMOVE_ITEM",
	UPDATE_PRODUCT_DATA: "UPDATE_PRODUCT_DATA",
};
const createAction = (type, payload = undefined) => ({type, payload});

export const increaseCountAction = productId =>
	createAction(ActionTypes.INCREASE_COUNT, productId);
export const decreaseCountAction = productId =>
	createAction(ActionTypes.DECREASE_COUNT, productId);
export const removeItemAction = productId =>
	createAction(ActionTypes.REMOVE_ITEM, productId);
export const updateItemAction = (id, data) =>
	createAction(ActionTypes.UPDATE_PRODUCT_DATA, {id: Number(id), data});

/**
 * @typedef Product
 * @property {number} id - Product identifier
 * @property {string} name - Product name
 * @property {number} price - Price of the product
 * @property {number} count - The number of current product inside user's cart
 */

function updateUserCart(productId, count) {
	const cart = JSON.parse(localStorage.getItem("products"));
	if (count === 0) {
		delete cart[productId];
	} else {
		cart[productId] = count;
	}
	localStorage.setItem("products", JSON.stringify(cart));
}

/**
 * Reducer for managing store of Checkout component
 *
 * @param {Product[]} state - List of products
 * @param {{type: string, payload: any}} action - Action to perform on state
 * @returns {Product[]} Updated state
 */
const CheckoutReducer = (state, action) => {
	switch (action.type) {
		case ActionTypes.INCREASE_COUNT:
		case ActionTypes.DECREASE_COUNT: {
			const productIndex = state.findIndex(({id}) => id === action.payload);
			/** @type Product */
			const newProduct = {
				...state[productIndex],
				count:
					state[productIndex].count +
					(action.type === ActionTypes.INCREASE_COUNT ? 1 : -1),
			};
			updateUserCart(newProduct.id, newProduct.count);
			return newProduct.count === 0
				? [...state.slice(0, productIndex), ...state.slice(productIndex + 1)]
				: [
						...state.slice(0, productIndex),
						newProduct,
						...state.slice(productIndex + 1),
				  ];
		}
		case ActionTypes.REMOVE_ITEM: {
			const productIndex = state.findIndex(({id}) => id === action.payload);
			updateUserCart(action.payload, 0);
			return [
				...state.slice(0, productIndex),
				...state.slice(productIndex + 1),
			];
		}
		case ActionTypes.UPDATE_PRODUCT_DATA: {
			const {id, data} = action.payload;
			const oldProductIndex = state.findIndex(p => p.id === id);
			return [
				...state.slice(0, oldProductIndex),
				{...state[oldProductIndex], ...data},
				...state.slice(oldProductIndex + 1),
			];
		}
	}
	return state;
};

/**
 * Initialize state for CheckoutReducer
 *
 * @param {string|null} jsonData - Contents of localStorage `products` key
 * @returns {Product[]} Initial state for CheckoutReducer
 */
export function initializeState(jsonData) {
	if (!jsonData) return [];
	const products = JSON.parse(jsonData);
	return Object.keys(products).map(key => ({
		id: Number(key),
		count: products[key],
	}));
}

export default CheckoutReducer;
