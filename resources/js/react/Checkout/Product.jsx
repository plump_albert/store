import React, {useEffect, useCallback, useMemo, useState} from "react";
import axios from "axios";
import {
	increaseCountAction,
	decreaseCountAction,
	removeItemAction,
	updateItemAction,
} from "./CheckoutReducer.js";

/**
 * @param props - React component props
 * @param {number} props.id - Product identifier
 * @param {string} props.name - Product name
 * @param {number} props.price - Price of the product
 * @param {number} props.count - The number of current product inside user's cart
 * @param {React.Dispatch} props.dispatch - Function to send actions to update state
 */
const Product = ({id, count, name, price, url, dispatch}) => {
	const [isLoading, setLoading] = useState(true);

	const onIncrement = useCallback(
		() => dispatch(increaseCountAction(id)),
		[dispatch, id]
	);
	const onDecrement = useCallback(
		() => dispatch(decreaseCountAction(id)),
		[dispatch, id]
	);
	const onDelete = useCallback(
		() => dispatch(removeItemAction(id)),
		[dispatch, id]
	);

	useEffect(() => {
		axios({
			method: "get",
			url: `/api/product?id=${id}`,
		})
			.then(({data}) => {
				dispatch(updateItemAction(id, {...data, price: Number(data.price)}));
			})
			.catch(e => console.error(e))
			.finally(() => setLoading(false));
	}, []);

	const loadingClass = useMemo(() => (isLoading ? "loading" : ""), [isLoading]);

	return (
		<div className="flex flex-row h-fit rounded">
			<div
				className={`mr-4 box-border rounded-xl flex-auto max-w-[128px] before:content-[''] before:float-left before:pt-[100%] relative ${loadingClass}`}
			>
				<img
					src={url}
					alt={name}
					className="absolute top-0 right-0 down-0 left-0 h-full w-full rounded-xl"
				/>
			</div>
			<div className="flex flex-col grow-[2] basis-0 align-baseline justify-between">
				<p className="mb-4 flex flex-col gap-2">
					<span
						className={`text-base text-black w-full min-h-[1.5rem] rounded capitalize ${loadingClass}`}
					>
						{name}
					</span>
					<span
						className={`text-xs text-gray-600 max-w-[4rem] rounded ${loadingClass}`}
					>
						{price && price.toFixed(2)} &#8381;
					</span>
				</p>
				<div className="flex flex-row justify-between">
					<div className="flex flex-row gap-2">
						<button
							className="material-icons-outlined rounded-lg border w-8 h-8 text-base text-black"
							onClick={onDecrement}
						>
							remove
						</button>
						<span className="w-8 h-8 text-base flex items-center justify-center text-black">
							{count}
						</span>
						<button
							className="material-icons-outlined rounded-lg border w-8 h-8 text-base text-black"
							onClick={onIncrement}
						>
							add
						</button>
					</div>
					<button
						className="material-icons-outlined rounded-lg bg-gray-100 w-8 h-8 text-base text-gray-600"
						onClick={onDelete}
					>
						delete
					</button>
				</div>
			</div>
		</div>
	);
};

export default Product;
