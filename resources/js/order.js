import axios from "axios";

const form = document.querySelector("form.order-form");
form.addEventListener("submit", e => {
	e.preventDefault();
	let products = localStorage.getItem("products");
	if (!products) window.location.replace("/checkout");
	products = JSON.parse(products);
	products = Object.keys(products).map(key => ({
		id: Number(key),
		count: products[key],
	}));
	let data = {
		email: form.email.value,
		first_name: form.first_name.value,
		last_name: form.last_name.value,
		address: form.address.value,
		products,
	};
	axios.post("/order", data).then(result => {
		if (result.status === 200 && result.data.success)
			window.location.pathname = "/order/" + result.data.id;
		else window.location.replace("/checkout");
	});
});

// Redirect user if there is no products in cart
let products = localStorage.getItem("products");
if (!products) window.location.replace("/checkout");
