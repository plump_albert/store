<div class="flex flex-row items-center justify-between gap-4 py-6">
	<a
		href="{{ url('/') }}"
		class="material-icons-outlined"
	> home </a>
	<h1 class="text-center font-serif text-2xl font-bold uppercase text-black">
		@yield("title", "Omigurumi Store") </h1>
	<a
		id="cart-icon"
		href="{{ url('/checkout') }}"
		class="material-icons-outlined cart-icon relative"
	> shopping_cart </a>
</div>
