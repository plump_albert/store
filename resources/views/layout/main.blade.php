<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta
		http-equiv="X-UA-Compatible"
		content="IE=edge"
	>
	<meta
		name="viewport"
		content="width= device-width , initial-scale= 1.0 "
	>
	<title> @yield("title", "Omigurumi Store") </title>
	<link
		rel="preconnect"
		href="https://fonts.googleapis.com"
	>
	<link
		rel="preconnect"
		href="https://fonts.gstatic.com"
		crossorigin
	>
	<link
		href="https://fonts.googleapis.com/css2?family=Lora:wght@400;700&family=Roboto:wght@400;500&display=swap"
		rel="stylesheet"
	>
	<link
		href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined"
		rel="stylesheet"
	>
	<link
		rel="stylesheet"
		href=" {{ url('css/app.css') }} "
	>
	@yield("head")
</head>

<body>
	<div class="flex min-h-full w-full flex-col px-4">
		<x-app-header></x-app-header>
		@yield("body")
	</div>
</body>

</html>
