@extends('layout.main')
@section('body')
	<form
		class="order-form relative flex flex-col gap-6"
		action="/order"
		method="POST"
	>
		@csrf
		<div class="relative flex flex-col">
			<label
				for="email"
				class="text-base font-medium"
			>Электронная почта</label>
			<input
				required
				type="email"
				name="email"
				id="email"
				class="rounded border text-base leading-normal"
			>
		</div>
		<div class="relative flex flex-col">
			<label
				for="last_name"
				class="text-base font-medium"
			>Фамилия</label>
			<input
				required
				type="text"
				name="last_name"
				id="last_name"
				class="rounded border text-base leading-normal"
			>
		</div>
		<div class="relative flex flex-col">
			<label
				for="first_name"
				class="text-base font-medium"
			>Имя</label>
			<input
				required
				type="text"
				name="first_name"
				id="first_name"
				class="rounded border text-base leading-normal"
			>
		</div>
		<div class="relative flex flex-col">
			<label
				for="address"
				class="text-base font-medium"
			>Адрес</label>
			<input
				type="text"
				name="address"
				id="address"
				class="rounded border text-base leading-normal"
			>
		</div>
		<input
			class="rounded bg-rose-600 py-2 px-4 font-medium text-white"
			type="submit"
			value="Заказать"
		>
	</form>
	</div>
	<script
	 defer
	 src="{{ url('js/order.js') }}"
	></script>
@endsection
