@extends("layout.main")
@section('head')
	<link
		rel="stylesheet"
		href="{{ url('css/checkout.css') }}"
	>
@endsection
@section('body')
	<div class="cart-container flex flex-1 flex-col pt-4 pb-8">
		<h3 class="text-center font-sans text-xl font-medium capitalize text-black">
			Корзина </h3>
		<div class="flex flex-1 flex-col justify-between overflow-hidden">
			<main class="product-list mt-6 flex flex-1 flex-col overflow-auto"></main>
		</div>
	</div>
	<script
	 defer
	 src="{{ url('js/checkout.js') }}"
	></script>
@endsection
