@extends('layout.main')
@section('body')
	<div
		class="my-6 grid grid-flow-row grid-cols-1 gap-x-4 gap-y-6 md:grid-cols-2 lg:grid-cols-3"
	>
		@foreach ($products as $product)
			<div class="align-center box-border flex flex-col rounded border p-4">
				<div class="relative mb-4 w-full before:block before:pt-[100%]">
					<img
						src="{{ isset($product->url) && $product->url }}"
						alt="{{ $product->name }}.png"
						class="absolute top-0 bottom-0 left-0 right-0 rounded"
					/>
				</div>
				<div class="flex flex-row items-center justify-between">
					<div class="flex flex-col items-start justify-between">
						<h3 class="text-base font-medium uppercase"> {{ $product->name }} </h3>
						<span class="text-xs font-medium text-gray-600"> ${{ $product->price }}
						</span>
					</div>
					<span
						class="material-icons-outlined product-icon text-black"
						data-id="{{ $product->id }}"
						onclick="productCartClick(this, {{ $product->id }})"
					>
						add_shopping_cart
					</span>
				</div>
			</div>
		@endforeach
	</div>
	@if ($products->lastPage() > 1)
		<nav class="mt-auto mb-6 w-full">
			<ul class="mx-5 flex flex-1 flex-row justify-between">
				@if ($products->currentPage() == 1)
					<li class="page-link text-gray-400">
						<span class="material-icons-outlined"> first_page </span>
					</li>
				@else
					<li
						class="page-link text-inherit"
						aria-label="First page"
						aria-placeholder="Go to first page"
						title="Go to first page"
					>
						<a
							href="{{ $products->url(1) }}"
							class="material-icons-outlined"
						> first_page </a>
					</li>
				@endif
				@for ($i = 1; $i <= $products->lastPage(); $i++)
					<?php
					$half_total_links = 2;
					$from = $products->currentPage() - $half_total_links;
					$to = $products->currentPage() + $half_total_links;
					if ($products->currentPage() < $half_total_links) {
					    $to += $half_total_links - $products->currentPage();
					}
					if ($products->lastPage() - $products->currentPage() < $half_total_links) {
					    $from -= $half_total_links - ($products->lastPage() - $products->currentPage()) - 1;
					}
					?>
					@if ($from < $i && $i < $to)
						<li
							class="page-link {{ $products->currentPage() == $i ? ' font-medium text-blue-600' : ' text-gray-400' }}"
						>
							<a href="{{ $products->url($i) }}">{{ $i }}</a>
						</li>
					@endif
				@endfor

				@if ($products->currentPage() == $products->lastPage())
					<li class="page-link text-gray-400">
						<span class="material-icons-outlined"> last_page </span>
					</li>
				@else
					<li
						class="page-link text-inherit"
						title="Go to last page"
						aria-label="Last page"
						aria-placeholder="Go to last page"
					>
						<a
							href="{{ $products->url($products->lastPage()) }}"
							class="material-icons-outlined"
						> last_page
						</a>
					</li>
				@endif
			</ul>
		</nav>
	@endif
	<script
	 defer
	 src="{{ url('js/index.js') }}"
	></script>
@endsection
