@extends('layout.main')
@section('body')
	<div class="m-auto flex flex-col items-center gap-6">
		<div class="w-full max-w-[96px]">
			<svg
				viewBox="0 0 24 24"
				class="material-icons-outlined fill-emerald-600"
			>
				<text
					x="0"
					y="24"
				> check </text>
			</svg>
		</div>
		<p class="text-center text-base">
			Ваш заказ №{{ $order->id }} на доставку по адресу "{{ $order->address }}"
			был принят.
		</p>
		<a
			href="/"
			class="text-rose-600"
		>Вернуться к покупкам</a>
	</div>
@endsection
