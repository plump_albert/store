<div class="flex flex-row rounded">
	<img
		src="{{ $product->url }}"
		alt="{{ $product->name }}"
		class="h-[64px] w-[64px]"
	/>
	<div class="flex flex-col align-baseline">
		<p class="mb-4 flex flex-col gap-2">
			<span class="text-base text-black">{{ $product->name }}</span>
			<span class="text-xs text-gray-600">{{ $product->price }}</span>
		</p>
		<div class="flex flex-row justify-between">
			<div class="flex flex-row gap-2">
				<button
					class="material-icons-outlined rounded-lg p-3 text-base text-black">remove</button>
				<span class="p-3 text-base text-black">{{ $count }}</span>
				<button
					class="material-icons-outlined rounded-lg p-3 text-base text-black">add</button>
			</div>
			<button
				class="material-icons-outlined rounded-lg p-3 text-base text-black">delete</button>
		</div>
	</div>
</div>
